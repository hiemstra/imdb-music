
import json
import random
import sys

db_artist_name = dict()
db_artist_type = dict()
db_artist_reverse = dict()
db_album_name = dict()

def random_id():
    id = 'IMDB'
    all = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    for i in range(18):
        r = int(random.random() * 62)
        id +=  all[r]
    return id

def process_performers(track_id, performers):
    for performer in performers:
        artist_id = performer['uri'].replace('spotify:artist:', '')
        name = performer['name'].replace("'", "''")
        if 'type' in performer:
            db_artist_type[artist_id] = performer['type'] # always 'artist'
        if artist_id in db_artist_name:
            if name != db_artist_name[artist_id]:
                 print(f'Warning: ambigious name for {artist_id} ({name} vs. {db_artist_name[artist_id]}', file=sys.stderr)
        else:
            db_artist_name[artist_id] = name
        print (f"INSERT INTO Performs(track_id, artist_id) VALUES ('{track_id}', '{artist_id}');")


def get_artist_id(name, person):
    if 'uri' in person:
        artist_id = person['uri'].replace('spotify:artist:', '')
    else:
        if name in db_artist_reverse:
             artist_id = db_artist_reverse[name]
        else:
             artist_id = random_id()
             db_artist_reverse[artist_id] = name
    return artist_id

def process_writers(track_id, writers):
    for writer in writers:
        name = writer['name'].replace("'", "''")
        artist_id = get_artist_id(name, writer)
        if artist_id in db_artist_name:
            if name != db_artist_name[artist_id]:
                 print(f'Warning: ambigious name for {artist_id} ({name} vs. {db_artist_name[artist_id]}', file=sys.stderr)
        else:
            db_artist_name[artist_id] = name
        roles = []
        if 'subroles' in writer:
            for role in writer['subroles']:
                if role == 'lyricist' or role == 'composer':
                    roles.append(role)
                elif role == 'writer' or role == 'author':
                    roles.append('lyricist')
                    roles.append('composer')
                else:
                    print(f'Warning: Unknown writer role "{role}"', file=sys.stderr)
        if roles:
            for role in roles:
                print (f"INSERT INTO Writes(artist_id, track_id, role) VALUES ('{artist_id}', '{track_id}', '{role}');")
        else:
            print (f"INSERT INTO Writes(artist_id, track_id) VALUES ('{artist_id}', '{track_id}');")


def process_producers(track_id, producers):
    for producer in producers:
        name = producer['name'].replace("'", "''")
        artist_id = get_artist_id(name, producer)
        if artist_id in db_artist_name:
            if name != db_artist_name[artist_id]:
                 print(f'Warning: ambigious name for {artist_id} ({name} vs. {db_artist_name[artist_id]}', file=sys.stderr)
        else:
            db_artist_name[artist_id] = name
        print (f"INSERT INTO Produces(artist_id, track_id) VALUES ('{artist_id}', '{track_id}');")


def process_markets(album_id, markets):
    count = len(markets)
    if count == 0:
        return
    i = int(random.random() * count)
    random_market = markets[i]
    my_markets = ['NL', 'US', 'GB', 'DE', 'ES', 'ZA']
    j = int(random.random() * len(my_markets))
    if not(random_market in my_markets):
        my_markets[j] = random_market
    random_market = markets[i]
    if not(random_market in my_markets):
        my_markets.append(random_market)
    for market in markets:
        if market in my_markets:
            print(f"INSERT INTO Market(album_id, country) VALUES ('{album_id}', '{market}');")


def process_album(album):
    album_id = album['id']
    if album_id in db_album_name:
        return album_id
    name = album['name'].replace("'", "''")
    db_album_name[album_id] = name
    release_date = album['release_date']
    year = release_date[:4]
    album_type = album['album_type'] 
    total_tracks = album['total_tracks'] 
    print(f"INSERT INTO Album(album_id, album_title, total_tracks, year, type) VALUES ('{album_id}', '{name}', {total_tracks}, {year}, '{album_type}');")
    process_markets(album_id, album['available_markets'])
    return album_id


with open("credits.json", "r") as file:
    for line in file:
        if 'trackUri' in line:
            credit = json.loads(line)
            track_id = credit['trackUri'].replace('spotify:track:', '')
            for role in credit['roleCredits']:
                title = role['roleTitle']
                if title == 'Writers':
                    process_writers(track_id, role['artists'])
                elif title == 'Producers':
                    process_producers(track_id, role['artists'])
                # Done below:
                #elif title == 'Performers':
                #    process_performers(track_id, role['artists'])

for nr in range(3):
    with open(f"playlist{nr}.json", "r") as file:
        playlist = json.loads(file.read())
        if 'tracks' in playlist:
            playlist = playlist['tracks']
        for item in playlist['items']:
            track = item['track']
            track_id = track['id']
            album_id = process_album(track['album'])
            process_performers(track_id, track['artists'])

            name = track['name']
            name = name.replace("'", "''")
            explicit = False
            if 'explicit' in track:
                explicit = track['explicit']
            popularity = track['popularity']
            position = track['track_number']
            duration = track['duration_ms']
            duration //= 1000
            print(f"INSERT INTO BTrack(track_id, album_id, track_title, duration, popularity, explicit) VALUES ('{track_id}', '{album_id}', '{name}', {duration}, {popularity}, {explicit});")

for artist_id in db_artist_name:
    name = db_artist_name[artist_id]
    print(f"INSERT INTO Artist(artist_id, name) VALUES ('{artist_id}', '{name}');")          
