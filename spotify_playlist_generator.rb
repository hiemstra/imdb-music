# frozen_string_literal: true

require 'net/http'
require 'uri'

# Request class, used to send requests
# to the Spotify API
class Request
  def initialize(playlist, track, oauth)
    @uri = URI.parse("https://api.spotify.com/v1/users/spotify/playlists/#{playlist}/tracks?uris=#{track}")
    @request = Net::HTTP::Post.new(@uri)
    @request.content_type = 'application/json'
    @request['Authorization'] = "Bearer #{oauth}"
    @request['Accept'] = 'application/json'
  end

  def send
    req_options = {
      use_ssl: @uri.scheme == 'https'
    }

    response = Net::HTTP.start(@uri.hostname, @uri.port, req_options) do |http|
      http.request(@request)
    end

    response.code
  end
end

# SpotifyPlaylistGenerator class, used to generate a playlist from a database
class SpotifyPlaylistGenerator
  def initialize(oauth, playlist)
    @oauth = oauth
    @playlist = playlist
  end

  def add_track(track)
    request = Request.new(@playlist, track, @oauth)
    request.send
  end
end

# TrackList class, used to read a file and get the track id's
class TrackList
  attr_reader :tracks

  def initialize(file)
    @tracks = []
    process_file(file)
  end

  private

  def process_file(file)
    File.open(file, 'r').each_line do |line|
      line.chomp!
      case line[0]
      when 'h'
        @tracks << line[31, 22]
      when 's'
        @tracks << line[14, 22]
      else
        puts "Error reading song at line #{line}"
      end
    end
  end
end

# ------------------------------
# Main
# ------------------------------

puts 'Enter playlist id'
playlist_id = gets.chomp
puts 'Enter oauth token'
oauth = gets.chomp

spotify = SpotifyPlaylistGenerator.new(oauth, playlist_id)
puts 'Enter file name'
tracklist = TrackList.new(gets.chomp)

tracklist.tracks.each do |id|
  string = "spotify:track:#{id}"
  puts "Adding #{string}"
  puts "Code: #{spotify.add_track(string)}" # Add track to playlist, and use returned response code in the console for manual checking.
  sleep 0.3 # Can probably be removed / lowered, but I don't want to risk getting rate limited even more. (I already got rate limited once)
end
