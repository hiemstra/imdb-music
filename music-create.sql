CREATE TABLE Album (
   album_id char(22) PRIMARY KEY,
   album_title text,
   total_tracks integer,
   year numeric(4,0),
   type text
);
CREATE TABLE Track (
    track_id char(22) PRIMARY KEY,
    album_id char(22) REFERENCES Album(album_id),
    track_title text,
    duration integer,
    popularity numeric(3,0),
    explicit boolean
);
CREATE TABLE Artist (
    artist_id char(22) PRIMARY KEY,
    name text
);
CREATE TABLE Performs (
    artist_id char(22) REFERENCES Artist(artist_id),
    track_id char(22) REFERENCES Track(track_id)
);
CREATE TABLE Market (
    album_id char(22) REFERENCES Album(album_id),
    country text
);
CREATE TABLE Writes (
    artist_id char(22) REFERENCES Artist(artist_id),
    track_id char(22) REFERENCES Track(track_id),
    role text -- composer, lyrisist
);
CREATE TABLE Produces (
    artist_id char(22) REFERENCES Artist(artist_id),
    track_id char(22) REFERENCES Track(track_id)
);

