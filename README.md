IM&DB Music Database
====================

Start by [creating an Spotify App](https://developer.spotify.com/dashboard/applications),
and note down the Client ID and Client Secret. Then, read the stuff about
[Web API](https://developer.spotify.com/documentation/web-api/),
[Authorization](https://developer.spotify.com/documentation/general/guides/authorization/), and
[App settings](https://developer.spotify.com/documentation/general/guides/authorization/app-settings/).


Spotify playlist generator
--------------------------

First, get the playlist from Quiz 1. On Brightspace, go to "Activities" ->
"Quizzes", then on the dropdown next to Quiz 1, click "Grade", then choose
"Export to CSV". Sort the CSV file on column I (Q #), and for the rows
where I contains "11", get column P (Answer Match). Safe this as
`spotify_list.txt`.

Second, clean the playlist; do some manual checking and something like:

    cat spotify_list.txt | sed -e 's/\?si=.*//' \
    | sed -e 's/spotify:track:/https:\/\/open.spotify.com\/track\//' \
    | sort -u | sort -R >spotify-done.txt

Third, create an empty play list on Spotify. Click "+" somewhere at
"Your Library" and get the play list ID from the URL, for instance:
https://open.spotify.com/playlist/6wQxMnvZlBWvRf7N2IzL2q

Fourth, log in with [implicit grant](https://developer.spotify.com/documentation/web-api/tutorials/implicit-flow)
by pasting the url below in your browser (the redirect URL will contain
the OAuth token). Replace <CLIENTID> with your apps client id and make
sure that the redirect URL is added (http://localhost:8080 here).

    https://accounts.spotify.com/authorize?response_type=token&client_id=<CLIENTID>&scope=playlist-modify-public&redirect_uri=http%3A%2F%2Flocalhost%3A8080&state=0123456789ABCDEF

Fifth, run the upload script, enter playlist id, OAuth token, file name:

    ruby spotify_playlist_generator.rb

Many thanks to Jeroen Brinkhorst!


Music database generator
------------------------

Now, we use simple [Client Credentials](https://developer.spotify.com/documentation/general/guides/authorization/client-credentials/):

    echo -n "clientID:clientSecret" | openssl base64
    curl -X "POST" -H "Authorization: Basic <BASE64>" -d grant_type=client_credentials https://accounts.spotify.com/api/token

    {"access_token":"<TOKEN>","token_type":"Bearer","expires_in":3600}

Get the [IM&DB playlist](https://open.spotify.com/playlist/7hQhQRgU7mEPleh5SW78XZ):

    curl --request GET --url https://api.spotify.com/v1/playlists/7hQhQRgU7mEPleh5SW78XZ \ 
    --header 'Authorization: Bearer <TOKEN>' \
    --header 'Content-Type: application/json' >playlist0.json

    curl --request GET --url 'https://api.spotify.com/v1/playlists/7hQhQRgU7mEPleh5SW78XZ/tracks?offset=100&limit=100' \
    --header 'Authorization: Bearer <TOKEN>' \
    --header 'Content-Type: application/json' >playlist1.json

    curl --request GET --url 'https://api.spotify.com/v1/playlists/7hQhQRgU7mEPleh5SW78XZ/tracks?offset=200&limit=100' \
    --header 'Authorization: Bearer <TOKEN>' \
    --header 'Content-Type: application/json' >playlist2.json

    curl --request GET --url 'https://api.spotify.com/v1/playlists/7hQhQRgU7mEPleh5SW78XZ/tracks?offset=300&limit=100' \
    --header 'Authorization: Bearer <TOKEN>' \
    --header 'Content-Type: application/json' >playlist3.json

Not so nice: in the web app, click for every track "Show credits" and get the JSON response
from the webconsole. Copy them one by one in: credits.json
Then, convert the JSON data to SQL:

    python json2sql.py | sort -u | sed -e 's/BTrack/Track/g' >music-inserts.sql

Create the SQL database:

    psql -f music-create.sql
    psql -f music-inserts.sql

Old data from the [2022/2023 play list](https://open.spotify.com/playlist/10zlYyX25dwDYgVY9olkwP):

    music2022-10-05.sql

Tracks without writers (maybe missing from credits.json)

    SELECT * FROM Track WHERE Track.track_id NOT IN (SELECT track_id FROM Writes);

